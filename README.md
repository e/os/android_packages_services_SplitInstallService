# Split Install Service

The Split Install Service is a service run as system uid and responsible
for installing a split apk with the hidden `PackageManager.INSTALL_DONT_KILL_APP`
flag. 

## Build

To use the service, add into your project the jar built with the `mm splitinstall-lib` AOSP 
build system command.

## Usage

Then, a client can bind to it with the following:

```kotlin
val intent = Intent().apply {
    component = SplitInstall.SPLIT_INSTALL_SYSTEM_SERVICE
}
bindService(intent, serviceConnection, BIND_AUTO_CREATE)
```

Then, install a split apk:

```kotlin
service.installSplitModule("fake.package.name", "pathToSplitApk")
```
