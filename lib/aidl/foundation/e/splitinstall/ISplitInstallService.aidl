package foundation.e.splitinstall;

/**
* Interface towards the SplitInstallService.
*/
interface ISplitInstallService {

    /**
     * Method allowing an application bound to the SplitInstallService
     * to install a split module.
     * @param packageName: the package name of the application we want to install the split module
     *                     for.
     * @param modulePath: the path of the split module apk we want to install.
     */
    oneway void installSplitModule(in String packageName, in String modulePath);
}
