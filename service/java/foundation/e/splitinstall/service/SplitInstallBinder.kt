/*
 * Copyright ECORP SAS 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package foundation.e.splitinstall.service

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInstaller
import android.util.Log
import foundation.e.splitinstall.ISplitInstallService
import java.io.File

class SplitInstallBinder(
    val applicationContext: Context
) : ISplitInstallService.Stub() {

    companion object {
        const val TAG = SplitInstallService.TAG
    }

    override fun installSplitModule(packageName: String, modulePath: String) {

        Log.i(TAG, "installing $modulePath")
        val packageManager = applicationContext.packageManager
        val packageInstaller = packageManager.packageInstaller
        val params = PackageInstaller.SessionParams(PackageInstaller.SessionParams.MODE_INHERIT_EXISTING)
            .apply {
            setAppPackageName(packageName)
            setDontKillApp(true)
        }

        val sessionId = packageInstaller.createSession(params)
        val session = packageInstaller.openSession(sessionId)

        try {
            syncFile(session, File(modulePath))

            val callbackIntent = Intent(applicationContext, SplitInstallBroadcastReceiver::class.java)
            callbackIntent.action = SplitInstallBroadcastReceiver.MODULE_INSTALLED_INTENT_ACTION

            val flags = PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE
            val servicePendingIntent =
                PendingIntent.getBroadcast(applicationContext, sessionId, callbackIntent, flags)
            session.commit(servicePendingIntent.intentSender)
        } catch (e: Exception) {
            session.abandon()
            throw e
        } finally {
            session.close()
        }
    }

    private fun syncFile(session: PackageInstaller.Session, file: File) {
        val inputStream = file.inputStream()
        val outputStream = session.openWrite(file.nameWithoutExtension, 0, -1)
        inputStream.copyTo(outputStream)
        session.fsync(outputStream)
        inputStream.close()
        outputStream.close()
    }
}