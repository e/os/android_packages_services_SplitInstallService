/*
 * Copyright ECORP SAS 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package foundation.e.splitinstall.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class SplitInstallBroadcastReceiver : BroadcastReceiver() {

    companion object {
        const val TAG = SplitInstallService.TAG
        const val MODULE_INSTALLED_SUCCESS_STATUS = 0
        const val MODULE_INSTALLED_INTENT_ACTION = "foundation.e.apps.splitinstall.MODULE_INSTALLED"
        const val PM_INSTALL_EXTRA_STATUS_KEY = "android.content.pm.extra.STATUS"
        const val PM_INSTALL_EXTRA_PACKAGE_NAME_KEY = "android.content.pm.extra.PACKAGE_NAME"
        const val PM_INSTALL_EXTRA_STATUS_MESSAGE_KEY = "android.content.pm.extra.STATUS_MESSAGE"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action != MODULE_INSTALLED_INTENT_ACTION) {
            Log.w(TAG, "Unexpected intent received: $intent")
            return
        }

        val extras = intent.extras
        if (extras == null) {
            Log.e(TAG, "Expected extras for the $MODULE_INSTALLED_INTENT_ACTION action")
            return
        }

        if (extras.get(PM_INSTALL_EXTRA_STATUS_KEY) != MODULE_INSTALLED_SUCCESS_STATUS) {
            val statusMessage = extras.get(PM_INSTALL_EXTRA_STATUS_MESSAGE_KEY)
            Log.e(TAG, "Could not install a split apk: $statusMessage")
            return
        }

        val updatedPackage = extras.get(PM_INSTALL_EXTRA_PACKAGE_NAME_KEY)
        Log.i(TAG, "Package updated successfully: $updatedPackage")
    }
}