/*
 * Copyright ECORP SAS 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package foundation.e.splitinstall.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInstaller.Session
import android.content.pm.PackageInstaller.SessionParams
import android.os.Build
import android.os.IBinder
import android.util.Log
import foundation.e.splitinstall.service.R

import java.io.File

class SplitInstallService : Service() {

    companion object {
        const val TAG = "SplitInstallSysService"
    }

    override fun onBind(intent: Intent?): IBinder? {
        return SplitInstallBinder(applicationContext)
    }
}
